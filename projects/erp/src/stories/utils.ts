interface ErpStoryWrapperOptions {
  style: {
    width?: string;
    padding?: string;
    backgroundColor?: string;
  };
}

export const erpStoryWrapper = (
  story: any['template'],
  options?: ErpStoryWrapperOptions
): any['template'] => {
  let style = 'display: grid; justify-content: center; margin: 2em auto;';

  if (!options) {
    options = { style: {} };
  }

  if (!options.style.width) {
    options.style.width = 'auto';
  }

  if (!options.style.padding) {
    options.style.padding = '0px';
  }

  if (!options.style.backgroundColor) {
    options.style.backgroundColor = 'transparent';
  }

  style += 'grid-template-columns: minmax(0px, ' + options.style.width + ');';
  style += 'padding:' + options.style.padding + ';';
  style += 'background-color:' + options.style.backgroundColor + ';';

  style += 'max-width:100%;';

  return `<div style="${style}">${story}</div>`;
};
