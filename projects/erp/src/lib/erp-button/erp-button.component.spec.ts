import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ErpButtonComponent } from './erp-button.component';

describe('ErpButtonComponent', () => {
  let component: ErpButtonComponent;
  let fixture: ComponentFixture<ErpButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ErpButtonComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ErpButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
