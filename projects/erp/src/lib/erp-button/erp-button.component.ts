import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'lib-erp-button',
  templateUrl: './erp-button.component.html',
  styleUrls: ['./erp-button.component.css'],
})
export class ErpButtonComponent implements OnInit {
  @Input() text: string = '';
  @Input() background: string = '';

  ngOnInit(): void {}

  onClick(): void {
    console.log('Hello! Component Library...');
  }
}
