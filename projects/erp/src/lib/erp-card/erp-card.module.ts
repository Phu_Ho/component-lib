import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ErpCardComponent } from './erp-card.component';

@NgModule({
  declarations: [ErpCardComponent],
  imports: [CommonModule],
  exports: [ErpCardComponent],
})
export class ErpCardModule {}
